package et.gov.customs.serviceregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class CnServiceRegistryApplication {

  public static void main(String[] args) {
    SpringApplication.run(CnServiceRegistryApplication.class, args);
  }

}
